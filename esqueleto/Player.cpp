#include "stdafx.h"
#include "Player.h"
#include <tinyxml.h>

#include <params.h>

Player::Player() {
	RTTI_BEGIN
		RTTI_EXTEND(MOAIEntity2D)
	RTTI_END

	mLinearVelocity  = { 0.0f, 0.0f };
	mAngularVelocity = 0.0f;
}

Player::~Player() {

}

void Player::OnStart() {
	ReadParams("sample/params.xml", mParams);

	mRandomTarget = { static_cast<float>(rand() % 1024 - 512), static_cast<float>(rand() % 768 - 384) };
}

void Player::OnStop() {

}

void Player::OnUpdate(float step) {
	USVec2D dist = GetLoc();
	USVec2D distance = mRandomTarget - GetLoc();
	if (distance.Length() < 10) {
		mRandomTarget = { static_cast<float>(rand() % 1024 - 512), static_cast<float>(rand() % 768 - 384) };
	}

	// ARRIVE
	arrive = ArriveSteering(this, mRandomTarget);
	USVec2D acce = arrive.GetSteering();

	mLinearVelocity.mX += acce.mX * step;
	mLinearVelocity.mY += acce.mY * step;
	SetLoc(GetLoc() += mLinearVelocity * step);

	// ALIGN TO MOVEMENT
	mParams.targetPosition = mRandomTarget;
	alignToMovement = AlignToMovementSteering(this);
	float acceRot = alignToMovement.GetSteering();

	mAngularVelocity += acceRot * step;
	SetRot(GetRot() + mAngularVelocity * step);
}

void Player::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);

	MOAIDraw::DrawPoint(mRandomTarget.mX, mRandomTarget.mY);
	
	//arrive.DrawDebug();
}


// Lua configuration

void Player::RegisterLuaFuncs(MOAILuaState & state) {
	MOAIEntity2D::RegisterLuaFuncs(state);

	luaL_Reg regTable[] = {
		{ "setLinearVel",			_setLinearVel },
		{ "setAngularVel",			_setAngularVel },
		{ NULL, NULL }
	};

	luaL_register(state, 0, regTable);
}

int Player::_setLinearVel(lua_State* L) {
	MOAI_LUA_SETUP(Player, "U")

	float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetLinearVelocity(pX, pY);
	return 0;
}

int Player::_setAngularVel(lua_State* L) {
	MOAI_LUA_SETUP(Player, "U")

	float angle = state.GetValue<float>(2, 0.0f);
	self->SetAngularVelocity(angle);

	return 0;
}
