#pragma once

#include "uslscore\USVec2D.h"
#include "params.h"

class Character;

class ObstacleAvoidanceSteering {
public:
	ObstacleAvoidanceSteering() {}
	ObstacleAvoidanceSteering(Character *character);
	USVec2D GetSteering();
	void DrawDebug();

	Character *mCharacter;
	USVec2D mAcce;
};
