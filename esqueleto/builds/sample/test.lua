MOAISim.openWindow("game", 1024, 768)

viewport = MOAIViewport.new()
viewport:setSize (1024, 768)
viewport:setScale (1024, -768)

layer = MOAILayer2D.new()
layer:setViewport(viewport)
MOAISim.pushRenderPass(layer)

-- DRAGON
texture_name = "sample/dragon.png"
gfxQuad = MOAIGfxQuad2D.new()
gfxQuad:setTexture(texture_name)
char_size = 64
gfxQuad:setRect(-char_size/2, -char_size/2, char_size/2, char_size/2)
gfxQuad:setUVRect(0, 0, 1, 1)
  
prop = MOAIProp2D.new()
prop:setDeck(gfxQuad)

entity = Character.new()
-- Add prop to be the renderable for this character
entity:setProp(prop, layer)
-- Start the character (allow calls to OnUpdate)
entity:start()
entity:setLoc(-400, -200)
entity:setRot(0)
entity:setLinearVel(20, 30)
entity:setAngularVel(30)

-- MAGO
--[[ texture_name_mago = "mago.png"
gfxQuadMago = MOAIGfxQuad2D.new()
gfxQuadMago:setTexture(texture_name_mago)
char_size_mago = 64
gfxQuadMago:setRect(-char_size_mago/2, -char_size_mago/2, char_size_mago/2, char_size_mago/2)
gfxQuadMago:setUVRect(0, 0, 1, 1)
  
propMago = MOAIProp2D.new()
propMago:setDeck(gfxQuadMago)

entityMago = Player.new()
-- Add prop to be the renderable for this character
entityMago:setProp(propMago, layer)
-- Start the character (allow calls to OnUpdate)
entityMago:start()
entityMago:setLoc(-200, -100)
entityMago:setRot(0)
entityMago:setLinearVel(10, 20)
entityMago:setAngularVel(30)

-- FANTASMA
texture_name_ghost = "ghost32.png"
gfxQuadGhost = MOAIGfxQuad2D.new()
gfxQuadGhost:setTexture(texture_name_ghost)
char_size_ghost = 64
gfxQuadGhost:setRect(-char_size_ghost/2, -char_size_ghost/2, char_size_ghost/2, char_size_ghost/2)
gfxQuadGhost:setUVRect(0, 0, 1, 1)
  
propGhost = MOAIProp2D.new()
propGhost:setDeck(gfxQuadGhost)

entityGhost = Enemy.new()
-- Add prop to be the renderable for this character
entityGhost:setProp(propGhost, layer)
-- Start the character (allow calls to OnUpdate)
entityGhost:start()
entityGhost:setLoc(-200, -100)
entityGhost:setRot(0)
entityGhost:setLinearVel(10, 20)
entityGhost:setAngularVel(30)
entityGhost:setTarget(entityMago) ]]--

-- Enable Debug Draw
debug = MOAIDrawDebug.get();
layer:setDrawDebug(debug)
-- Add this character to draw debug
MOAIDrawDebug.insertEntity(entity)
--MOAIDrawDebug.insertEntity(entityMago)
--MOAIDrawDebug.insertEntity(entityGhost)

mouseX = 0
mouseY = 0

function onClick(down)
  entity:setLoc(mouseX, mouseY)
  entity:setRot(-135)
end

function pointerCallback(x, y)
    mouseX, mouseY = layer:wndToWorld(x, y)
end

MOAIInputMgr.device.mouseLeft:setCallback(onClick)
MOAIInputMgr.device.pointer:setCallback(pointerCallback)
