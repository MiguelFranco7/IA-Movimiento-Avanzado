#include "stdafx.h"
#include "ArriveSteering.h"
#include "character.h"

ArriveSteering::ArriveSteering(Character * character, USVec2D target) {
	mCharacter = character;
	mTarget    = target;
}

USVec2D ArriveSteering::GetSteering() {
	mDreamVelocity = mTarget - mCharacter->GetLoc();

	float distance = mDreamVelocity.Length();
	float radius = mCharacter->GetParams().arrive_radius;
	mDreamVelocity.NormSafe();
	mDreamVelocity.Scale(mCharacter->GetParams().max_velocity);

	if (distance < radius) {
		mDreamVelocity.Scale(distance / radius);
	}

	mAcce = mDreamVelocity - mCharacter->GetLinearVelocity();
	mAcce.Scale(mCharacter->GetParams().max_acceleration);

	return mAcce;
}

void ArriveSteering::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);

	// Vector (L�nea) de la velocidad deseada calculada en el frame anterior
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), USVec2D(mCharacter->GetLoc().mX + mDreamVelocity.mX, mCharacter->GetLoc().mY + mDreamVelocity.mY));

	gfxDevice.SetPenColor(0.0f, 1.0f, 0.0f, 0.5f);
	// Vector (L�nea) de la aceleraci�n calculada en el frame anterior 
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), USVec2D(mCharacter->GetLoc().mX + mAcce.mX, mCharacter->GetLoc().mY + mAcce.mY));
}
