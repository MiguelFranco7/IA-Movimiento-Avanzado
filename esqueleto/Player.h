#pragma once

#include "character.h"

class Player : public Character {
public:
	DECL_LUA_FACTORY(Player)
protected:
	virtual void OnStart();
	virtual void OnStop();
	virtual void OnUpdate(float step);
public:
	virtual void DrawDebug();
	Player();
	~Player();

private:
	USVec2D mRandomTarget;

	// Lua configuration
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);
private:
	static int _setLinearVel(lua_State* L);
	static int _setAngularVel(lua_State* L);
};