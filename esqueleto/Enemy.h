#pragma once

#include "character.h"

class Enemy : public Character {
public:
	DECL_LUA_FACTORY(Enemy)
protected:
	virtual void OnStart();
	virtual void OnStop();
	virtual void OnUpdate(float step);
public:
	virtual void DrawDebug();
	Enemy();
	~Enemy();

private:
	//USVec2D mLinearVelocity;
	//float mAngularVelocity;

	/*Params			  mParams;
	SeekSteering	  seek;
	ArriveSteering	  arrive;
	AlignSteering	  align;
	PursueSteering	  pursue;*/
	static Character *mTarget;

	// Lua configuration
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);
private:
	static int _setLinearVel(lua_State* L);
	static int _setAngularVel(lua_State* L);
	static int _setTarget(lua_State* L);
};