#include "stdafx.h"
#include "AlignSteering.h"
#include "character.h"

AlignSteering::AlignSteering(Character * character, float target) {
	mCharacter = character;
	mTarget    = target;
}

float AlignSteering::GetSteering() {
	mDreamVelocity = mTarget - mCharacter->GetRot();

	while (mDreamVelocity < -180)
		mDreamVelocity += 360;

	while (mDreamVelocity > 180)
		mDreamVelocity -= 360;

	mDreamVelocity /= 180;
	mDreamVelocity *= mCharacter->GetParams().max_angular_velocity;

	/*if (mDreamVelocity > 180)
	mDreamVelocity /= 180;
	else if (mDreamVelocity < -180)
	mDreamVelocity /= 180;*/

	mAcce = mDreamVelocity - mCharacter->GetAngularVelocity();
	mAcce *= mCharacter->GetParams().max_angular_acceleration;

	return mAcce;
}

void AlignSteering::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);
}
