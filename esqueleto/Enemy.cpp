#include "stdafx.h"
#include "Enemy.h"
#include "Player.h"
#include <tinyxml.h>

#include <params.h>

Character* Enemy::mTarget;

Enemy::Enemy() {
	RTTI_BEGIN
		RTTI_EXTEND(MOAIEntity2D)
	RTTI_END

	mLinearVelocity  = { 0.0f, 0.0f };
	mAngularVelocity = 0.0f;
}

Enemy::~Enemy() {

}

void Enemy::OnStart() {
	ReadParams("sample/enemy_params.xml", mParams);
}

void Enemy::OnStop() {

}

void Enemy::OnUpdate(float step) {
	USVec2D distancePlayer = mTarget->GetLoc() - GetLoc();
	if (distancePlayer.Length() < 10) {
		SetLoc({ 0.0f, 0.0f, 0.0f });
	}

	// PURSUE
	pursue = PursueSteering(this, mTarget);
	USVec2D acce = pursue.GetSteering();

	mLinearVelocity.mX += acce.mX * step;
	mLinearVelocity.mY += acce.mY * step;
	SetLoc(GetLoc() += mLinearVelocity * step);

	// ALIGN TO MOVEMENT
	alignToMovement = AlignToMovementSteering(this);
	float acceRot = alignToMovement.GetSteering();

	mAngularVelocity += acceRot * step;
	SetRot(GetRot() + mAngularVelocity * step);
}

void Enemy::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(0.0f, 0.0f, 1.0f, 0.5f);

	MOAIDraw::DrawPoint(0.0f, 0.0f);
}


// Lua configuration

void Enemy::RegisterLuaFuncs(MOAILuaState & state) {
	MOAIEntity2D::RegisterLuaFuncs(state);

	luaL_Reg regTable[] = {
		{ "setLinearVel",			_setLinearVel },
		{ "setAngularVel",			_setAngularVel },
		{ "setTarget",				_setTarget },
		{ NULL, NULL }
	};

	luaL_register(state, 0, regTable);
}

int Enemy::_setLinearVel(lua_State* L) {
	MOAI_LUA_SETUP(Enemy, "U")

		float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetLinearVelocity(pX, pY);
	return 0;
}

int Enemy::_setAngularVel(lua_State* L) {
	MOAI_LUA_SETUP(Enemy, "U")

		float angle = state.GetValue<float>(2, 0.0f);
	self->SetAngularVelocity(angle);

	return 0;
}

int Enemy::_setTarget(lua_State * L) {
	MOAI_LUA_SETUP(Enemy, "U")

	mTarget = state.GetLuaObject<Player>(2, true);

	return 0;
}
