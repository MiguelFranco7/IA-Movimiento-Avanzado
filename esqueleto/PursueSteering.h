#pragma once

#include "ArriveSteering.h"
#include "SeekSteering.h"

class Character;

class PursueSteering {
public:
	PursueSteering() {}
	PursueSteering(Character *character, Character *target);
	USVec2D GetSteering(); // Calculo de la nueva aceleracion.
	void DrawDebug();

	Character *mCharacter;
	Character *mTarget;
	USVec2D    mAcce;
	USVec2D	   mDreamVelocity;
};